package ru.unn;

import com.opencsv.CSVReader;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class main {
    public static void main(String[] args) throws IOException {
        SqlSessionFactoryBuilder builder =  new SqlSessionFactoryBuilder();
        SqlSession session = builder.build( Resources.getResourceAsReader("config.xml")).openSession();
        CSVReader reader = new CSVReader(new FileReader("solar.csv"),',','"',0);
        String [] nextLine;
        while((nextLine=reader.readNext())!=null) {
            HashMap<String,Object> map = new HashMap<String, Object>();
            map.put("DATE",nextLine[2]+' ' +nextLine[3]);
            map.put("VAL",nextLine[4]);
            session.insert("insert",map);
            session.commit();
        }
        reader.close();
        session.close();
    }
}
