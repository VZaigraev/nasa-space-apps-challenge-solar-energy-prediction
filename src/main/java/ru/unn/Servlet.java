package ru.unn;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String date = req.getParameter("date");
        String eff = req.getParameter("eff");
        String area = req.getParameter("area");
        SqlSessionFactory factory = new SqlSessionFactoryBuilder()
                .build( Resources.getResourceAsReader("config.xml"));
        List<HashMap> list;
        try(SqlSession session = factory.openSession()) {
            HashMap<String,Object> map = new HashMap<>();
            map.put("val",date);
            list = session.selectList("selectData", map);
        }
        String val = null;
        try {
            val = serialize(list,eff,area);
            resp.getWriter().write(val);
            resp.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

    }
//Debug hint
//    public static void main(String[] args) throws IOException {
//        SqlSessionFactory factory = new SqlSessionFactoryBuilder()
//                .build( Resources.getResourceAsReader("config.xml"));
//        List<HashMap> list;
//        try(SqlSession session = factory.openSession()) {
//            HashMap<String,Object> map = new HashMap<>();
//            map.put("val","2016-12-20");
//            list = session.selectList("selectData", map);
//        }
//        System.out.println(serialize(list,"0.02","70"));
//    }

    private static String serialize(List<HashMap> list, String eff, String area) throws JsonProcessingException {
        List<Long> timestamps = new ArrayList<>();
        List<Double> values = new ArrayList<>();
        for (HashMap map: list) {
            timestamps.add(0,((Timestamp)map.get("datadate")).getTime());
            BigDecimal val = (BigDecimal) map.get("value");
            values.add(0,val.longValue()*Double.valueOf(area)*Double.valueOf(eff)/12);
        }
        HashMap<String,List> map = new HashMap<>();
        map.put("date",timestamps);
        map.put("val",values);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(map);
    }

}
